﻿namespace DomainApiConnector.Sample
{
    using System;
    using System.Linq;

    using DomainApiConnector;
    using DomainApiConnector.Models;
    using DomainApiConnector.Models.Enums;

    class Program
    {
        private static void Main(string[] args)
        {
            var clientId = "client-id-here";
            var clientSecret = "client-secret-here";

            var client = new Client(clientId, clientSecret);
            var request = ListingRequest.GetDefault();
            request.PageSize = 200;
            request.PropertyFeatures = new[] { PropertyFeature.PetsAllowed, PropertyFeature.Furnished };
            request.ListingType = ListingType.Rent; 
            request.ListingAttributes = new[] { ListingAttribute.HasPhotos };
            request.MinBathrooms = 1;
            request.MaxBathrooms = 3;
            request.MinCarspaces = "1";
            request.MaxPrice = "2000";
            request.Locations = new Location[] { new Location
                                                     {
                                                         State = "NSW",
                                                         Area = "North Shore - Lower",
                                                         Region = "Sydney Region",
                                                         Suburb = "Chatswood"
                                                     } };
            request.Sort.SortKey = SortKey.Price;
            var listing = client.GetListing(request);
            Console.WriteLine($"I retrieved '{listing.ListingItems.Count()}' item(s).");
            Console.ReadLine();
        }
    }
}
