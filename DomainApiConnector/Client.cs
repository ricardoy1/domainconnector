﻿namespace DomainApiConnector
{
    using System;
    using System.Collections.Generic;
    using System.Configuration;
    using System.Net.Http;
    using System.Net.Http.Headers;
    using System.Runtime.Caching;
    using System.Text;

    using Models;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Serialization;

    public class Client
    {
        public string ClientSecret { get; private set; }

        public string ClientId { get; private set; }

        public Client()
        {
            this.ClientId = ConfigurationManager.AppSettings["clientId"];
            this.ClientSecret = ConfigurationManager.AppSettings["clientSecret"];
        }

        public Client(string clientId, string clientSecret)
        {
            this.ClientSecret = clientSecret;
            this.ClientId = clientId;
        }

        public ListingResponse GetListing(ListingRequest request)
        {
            var requestContent = GetRequestContent(request);

            HttpResponseMessage response;

            var accessToken = this.GetAccessToken();

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", accessToken);
                response = httpClient.PostAsync("https://api.domain.com.au/v1/listings/_search", requestContent).Result;
            }

            var responseContent = response.Content.ReadAsStringAsync().Result;

            var listingResponse = new ListingResponse();

            if (!response.IsSuccessStatusCode)
            {
                listingResponse.Error = responseContent;
            }
            else
            {
              listingResponse.ListingItems = JsonConvert.DeserializeObject<IEnumerable<ListingItem>>(responseContent);
            }

            return listingResponse;
        }

        private static StringContent GetRequestContent(ListingRequest request)
        {
            var serializerSettings = new JsonSerializerSettings
                               {
                                   ContractResolver = new CamelCasePropertyNamesContractResolver(),
                                   NullValueHandling = NullValueHandling.Ignore
                               };
            var serializedRequest = JsonConvert.SerializeObject(request, Formatting.Indented, serializerSettings);
            var content = new StringContent(serializedRequest, Encoding.UTF8, "application/json");

            return content;
        }

        private TokenResponse GetNewToken(TokenRequest tokenRequest)
        {

            var tokenParameters = new Dictionary<string, string>
                            {
                                { "grant_type", tokenRequest.GrantType },
                                { "scope", tokenRequest.Scope }
                            };

            var requestContent = new FormUrlEncodedContent(tokenParameters);

            var tokenAuthorization = Convert.ToBase64String(
                new ASCIIEncoding().GetBytes($"{tokenRequest.ClientId}:{tokenRequest.ClientSecret}"));

            HttpResponseMessage response;

            using (var httpClient = new HttpClient())
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue(
                    "Basic",
                    tokenAuthorization);

                response = httpClient.PostAsync("https://auth.domain.com.au/v1/connect/token", requestContent).Result;
            }

            var responseContent = response.Content.ReadAsStringAsync().Result;
            var tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(responseContent);

            return tokenResponse;
        }

        private string GetAccessToken()
        {
            var cache = MemoryCache.Default;
            var token = (string)cache["accessToken"];
            if (token == null)
            {
                var tokenRequest = this.BuildTokenRequest();
                var tokenResponse = this.GetNewToken(tokenRequest);
                var expiration = new DateTimeOffset(DateTime.UtcNow.AddSeconds(tokenResponse.ExpiresIn));
                var policy = new CacheItemPolicy
                                 {
                                     AbsoluteExpiration = expiration
                                 };
                token = tokenResponse.AccessToken;
                cache.Add("accessToken", token, policy);
            }

            return token;
        }

        private TokenRequest BuildTokenRequest()
        {
            return new TokenRequest
                       {
                           ClientId = this.ClientId,
                           ClientSecret = this.ClientSecret,
                           Scope = "api_listings_read api_agencies_read",
                           GrantType = "client_credentials"
                       };
        }
    }
}
