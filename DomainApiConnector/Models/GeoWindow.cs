namespace DomainApiConnector.Models
{
    public class GeoWindow
    {
        public Box Box { get; set; }
        public Circle Circle { get; set; }
        public Polygon Polygon { get; set; }
    }
}