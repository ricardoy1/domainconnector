namespace DomainApiConnector.Models
{
    public class AuctionSchedule
    {
        public string Time { get; set; }
        public string AuctionLocation { get; set; }
    }
}