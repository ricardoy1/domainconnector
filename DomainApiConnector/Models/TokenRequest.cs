namespace DomainApiConnector.Models
{
    using Newtonsoft.Json;

    public class TokenRequest
    {
        public string ClientSecret;

        [JsonProperty("grant_type")]
        public string GrantType { get; set; }

        [JsonProperty("scope")]
        public string Scope { get; set; }

        public string ClientId { get; set; }
    }
}