namespace DomainApiConnector.Models
{
    using System.Collections.Generic;

    public class Polygon
    {
        public List<Point> Points { get; set; }
    }
}