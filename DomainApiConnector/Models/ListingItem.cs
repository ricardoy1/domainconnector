namespace DomainApiConnector.Models
{
    public class ListingItem
    {
        public string Type { get; set; }
        public Property Listing { get; set; }
    }
}