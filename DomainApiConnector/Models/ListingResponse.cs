﻿namespace DomainApiConnector.Models
{
    using System.Collections.Generic;

    public class ListingResponse
    {
        public IEnumerable<ListingItem> ListingItems { get; set; }

        public string Error { get; set; }
    }
}
