﻿namespace DomainApiConnector.Models
{
    public class Location
    {
        public string State { get; set; }
        public string Region { get; set; }
        public string Area { get; set; }
        public string Suburb { get; set; }
        public string PostCode { get; set; }
        public bool IncludeSurroundingSuburbs { get; set; }
    }
}
