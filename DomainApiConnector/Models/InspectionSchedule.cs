namespace DomainApiConnector.Models
{
    using System.Collections.Generic;

    public class InspectionSchedule
    {
        public bool ByAppointment { get; set; }
        public bool Recurring { get; set; }
        public List<object> Times { get; set; }
    }
}