namespace DomainApiConnector.Models
{
    using System.Collections.Generic;

    using DomainApiConnector.Models.Enums;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class ListingRequest
    {
        public static ListingRequest GetDefault()
        {
            var request = new ListingRequest();
            request.ListingType = ListingType.None;
            request.LocationTerms = "";
            request.InspectionFrom = "";
            request.InspectionTo = "";
            request.AuctionFrom = "";
            request.AuctionTo = "";
            request.MinBedrooms = -1;
            request.MaxBedrooms = -1;
            request.MinBathrooms = -1;
            request.MaxBathrooms = -1;
            request.Sort = new Sort { SortKey = SortKey.None, ProximityTo = new Point { Lat = -1, Lon = -1 } };
            request.GeoWindow = new GeoWindow
                                    {
                                        Box = new Box
                                                  {
                                                      BottomRight = new Point { Lat = -1, Lon = -1 },
                                                      TopLeft = new Point { Lat = -1, Lon = -1 }
                                                  },
                                        Circle = new Circle
                                                     {
                                                         Center = new Point { Lat = -1, Lon = -1 },
                                                         RadiusInMeters = ""
                                                     },
                                        Polygon = new Polygon()
                                    };
            request.MinCarspaces = "";
            request.MaxCarspaces = "";
            request.MinPrice = "";
            request.MaxPrice = "";
            request.MinLandArea = "";
            request.MaxLandArea = "";
            request.Page = 0;
            request.PageSize = 10;

            return request;
        }

        [JsonConverter(typeof(StringEnumConverter))]
        public ListingType ListingType { get; set; }
        [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
        public PropertyType[] PropertyTypes { get; set; }
        [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
        public PropertyFeature[] PropertyFeatures { get; set; }
        [JsonProperty(ItemConverterType = typeof(StringEnumConverter))]
        public ListingAttribute[] ListingAttributes { get; set; }
        public double MinBedrooms { get; set; }
        public double MaxBedrooms { get; set; }
        public double MinBathrooms { get; set; }
        public double MaxBathrooms { get; set; }
        public string MinCarspaces { get; set; }
        public string MaxCarspaces { get; set; }
        public string MinPrice { get; set; }
        public string MaxPrice { get; set; }
        public string MinLandArea { get; set; }
        public string MaxLandArea { get; set; }
        public string LocationTerms { get; set; }
        public List<string> Keywords { get; set; }
        public string InspectionFrom { get; set; }
        public string InspectionTo { get; set; }
        public string AuctionFrom { get; set; }
        public string AuctionTo { get; set; }
        public Sort Sort { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
        public GeoWindow GeoWindow { get; set; }
        public Location[] Locations { get; set; }
    }
}