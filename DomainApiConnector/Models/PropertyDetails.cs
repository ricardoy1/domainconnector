namespace DomainApiConnector.Models
{
    using System.Collections.Generic;

    using DomainApiConnector.Models.Enums;

    public class PropertyDetails
    {
        public string State { get; set; }
        public PropertyFeature[] Features { get; set; }
        public PropertyType PropertyType { get; set; }
        public PropertyType[] AllPropertyTypes { get; set; }
        public double Bathrooms { get; set; }
        public double Bedrooms { get; set; }
        public double Carspaces { get; set; }
        public string UnitNumber { get; set; }
        public string StreetNumber { get; set; }
        public string Street { get; set; }
        public string Area { get; set; }
        public string Region { get; set; }
        public string Suburb { get; set; }
        public string Postcode { get; set; }
        public string DisplayableAddress { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public List<string> OnlyShowProperties { get; set; }
        public double? LandArea { get; set; }
        public double? BuildingArea { get; set; }
    }
}