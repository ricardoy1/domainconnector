namespace DomainApiConnector.Models
{
    using System.Collections.Generic;

    using DomainApiConnector.Models.Enums;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class Property
    {
        public string PromoLevel { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public ListingType ListingType { get; set; }
        public int Id { get; set; }
        public Advertiser Advertiser { get; set; }
        public PriceDetails PriceDetails { get; set; }
        public List<Medium> Media { get; set; }
        public PropertyDetails PropertyDetails { get; set; }
        public string Headline { get; set; }
        public string SummaryDescription { get; set; }
        public bool HasFloorplan { get; set; }
        public bool HasVideo { get; set; }
        public List<string> Labels { get; set; }
        public AuctionSchedule AuctionSchedule { get; set; }
        public InspectionSchedule InspectionSchedule { get; set; }
        public string ListingSlug { get; set; }
    }
}