﻿namespace DomainApiConnector.Models
{
    using DomainApiConnector.Models.Enums;

    using Newtonsoft.Json;
    using Newtonsoft.Json.Converters;

    public class Sort
    {
        [JsonConverter(typeof(StringEnumConverter))]
        public SortKey SortKey { get; set; }
        [JsonConverter(typeof(StringEnumConverter))]
        public SortDirection Direction { get; set; }
        public Point ProximityTo { get; set; }
    }
}
