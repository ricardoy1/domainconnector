namespace DomainApiConnector.Models
{
    public class Medium
    {
        public string Category { get; set; }
        public string Url { get; set; }
    }
}