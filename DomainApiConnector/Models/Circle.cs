namespace DomainApiConnector.Models
{
    public class Circle
    {
        public Point Center { get; set; }
        public string RadiusInMeters { get; set; }
    }
}