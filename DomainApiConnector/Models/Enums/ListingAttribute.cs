namespace DomainApiConnector.Models.Enums
{
    using System.Runtime.Serialization;

    public enum ListingAttribute
    {
        [EnumMember(Value = "")]
        None,
        [EnumMember(Value = "HasPhotos")]
        HasPhotos,
        [EnumMember(Value = "HasPrice")]
        HasPrice,
        [EnumMember(Value = "NotUpForAuction")]
        NotUpForAuction,
        [EnumMember(Value = "NotUnderContract")]
        NotUnderContract,
        [EnumMember(Value = "MarkedAsNew")]
        MarkedAsNew
    }
}