namespace DomainApiConnector.Models.Enums
{
    using System.Runtime.Serialization;

    public enum SortKey
    {
        [EnumMember(Value = "")]
        None,
        [EnumMember(Value = "Default")]
        Default,
        [EnumMember(Value = "Suburb")]
        Suburb,
        [EnumMember(Value = "Price")]
        Price,
        [EnumMember(Value = "DateUpdated")]
        DateUpdated,
        [EnumMember(Value = "InspectionTime")]
        InspectionTime,
        [EnumMember(Value = "Proximity")]
        Proximity,
        [EnumMember(Value = "SoldDate")]
        SoldDate
    }
}