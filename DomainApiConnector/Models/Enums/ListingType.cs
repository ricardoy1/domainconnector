namespace DomainApiConnector.Models.Enums
{
    using System.Runtime.Serialization;

    public enum ListingType
    {
        [EnumMember(Value = "")]
        None,
        [EnumMember(Value = "Sale")]
        Sale,
        [EnumMember(Value = "Rent")]
        Rent,
        [EnumMember(Value = "Share")]
        Share,
        [EnumMember(Value = "Sold")]
        Sold,
        [EnumMember(Value = "NewHomes")]
        NewHomes
    }
}