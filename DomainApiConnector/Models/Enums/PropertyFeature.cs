namespace DomainApiConnector.Models.Enums
{
    using System.Runtime.Serialization;

    public enum PropertyFeature
    {
        [EnumMember(Value = "AirConditioning")]
        AirConditioning,
        [EnumMember(Value = "BuiltInWardrobes")]
        BuiltInWardrobes,
        [EnumMember(Value = "CableOrSatellite")]
        CableOrSatellite,
        [EnumMember(Value = "Ensuite")]
        Ensuite,
        [EnumMember(Value = "Floorboards")]
        Floorboards,
        [EnumMember(Value = "Gas")]
        Gas,
        [EnumMember(Value = "InternalLaundry")]
        InternalLaundry,
        [EnumMember(Value = "PetsAllowed")]
        PetsAllowed,
        [EnumMember(Value = "SecureParking")]
        SecureParking,
        [EnumMember(Value = "SwimmingPool")]
        SwimmingPool,
        [EnumMember(Value = "Furnished")]
        Furnished,
        [EnumMember(Value = "GroundFloor")]
        GroundFloor,
        [EnumMember(Value = "WaterViews")]
        WaterViews,
        [EnumMember(Value = "NorthFacing")]
        NorthFacing,
        [EnumMember(Value = "CityViews")]
        CityViews,
        [EnumMember(Value = "IndoorSpa")]
        IndoorSpa,
        [EnumMember(Value = "Gym")]
        Gym,
        [EnumMember(Value = "AlarmSystem")]
        AlarmSystem,
        [EnumMember(Value = "Intercom")]
        Intercom,
        [EnumMember(Value = "BroadbandInternetAccess")]
        BroadbandInternetAccess,
        [EnumMember(Value = "Bath")]
        Bath,
        [EnumMember(Value = "Fireplace")]
        Fireplace,
        [EnumMember(Value = "SeparateDiningRoom")]
        SeparateDiningRoom,
        [EnumMember(Value = "Heating")]
        Heating,
        [EnumMember(Value = "Dishwasher")]
        Dishwasher,
        [EnumMember(Value = "Study")]
        Study,
        [EnumMember(Value = "TennisCourt")]
        TennisCourt,
        [EnumMember(Value = "Shed")]
        Shed,
        [EnumMember(Value = "FullyFenced")]
        FullyFenced,
        [EnumMember(Value = "BalconyDeck")]
        BalconyDeck,
        [EnumMember(Value = "GardenCourtyard")]
        GardenCourtyard,
        [EnumMember(Value = "OutdoorSpa")]
        OutdoorSpa,
        [EnumMember(Value = "DoubleGlazedWindows")]
        DoubleGlazedWindows,
        [EnumMember(Value = "EnergyEfficientAppliances")]
        EnergyEfficientAppliances,
        [EnumMember(Value = "WaterEfficientAppliances")]
        WaterEfficientAppliances,
        [EnumMember(Value = "WallCeilingInsulation")]
        WallCeilingInsulation,
        [EnumMember(Value = "RainwaterStorageTank")]
        RainwaterStorageTank,
        [EnumMember(Value = "GreywaterSystem")]
        GreywaterSystem,
        [EnumMember(Value = "WaterEfficientFixtures")]
        WaterEfficientFixtures,
        [EnumMember(Value = "SolarHotWater")]
        SolarHotWater,
        [EnumMember(Value = "SolarPanels")]
        SolarPanels
    }
}