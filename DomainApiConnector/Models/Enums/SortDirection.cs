namespace DomainApiConnector.Models.Enums
{
    using System.Runtime.Serialization;

    public enum SortDirection
    {
        [EnumMember(Value = "")]
        None,
        [EnumMember(Value = "Ascending")]
        Ascending,
        [EnumMember(Value = "Descending")]
        Descending
    }
}