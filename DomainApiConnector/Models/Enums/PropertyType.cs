namespace DomainApiConnector.Models.Enums
{
    using System.Runtime.Serialization;

    public enum PropertyType
    {
        [EnumMember(Value = "")]
        None,

        [EnumMember(Value = "AcreageSemiRural")]
        AcreageSemiRural,

        [EnumMember(Value = "ApartmentUnitFlat")]
        ApartmentUnitFlat,

        [EnumMember(Value = "BlockOfUnits")]
        BlockOfUnits,

        [EnumMember(Value = "CarSpace")]
        CarSpace,

        [EnumMember(Value = "DevelopmentSite")]
        DevelopmentSite,

        [EnumMember(Value = "Duplex")]
        Duplex,

        [EnumMember(Value = "Farm")]
        Farm,

        [EnumMember(Value = "NewHomeDesigns")]
        NewHomeDesigns,

        [EnumMember(Value = "House")]
        House,

        [EnumMember(Value = "NewHouseLand")]
        NewHouseLand,

        [EnumMember(Value = "NewLand")]
        NewLand,

        [EnumMember(Value = "NewApartments")]
        NewApartments,

        [EnumMember(Value = "Penthouse")]
        Penthouse,

        [EnumMember(Value = "RetirementVillage")]
        RetirementVillage,

        [EnumMember(Value = "Rural")]
        Rural,

        [EnumMember(Value = "SemiDetached")]
        SemiDetached,

        [EnumMember(Value = "SpecialistFarm")]
        SpecialistFarm,

        [EnumMember(Value = "Studio")]
        Studio,

        [EnumMember(Value = "Terrace")]
        Terrace,

        [EnumMember(Value = "Townhouse")]
        Townhouse,

        [EnumMember(Value = "VacantLand")]
        VacantLand,

        [EnumMember(Value = "Villa")]
        Villa,

        [EnumMember(Value = "Unknown")]
        Unknown
    }
}