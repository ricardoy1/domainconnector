namespace DomainApiConnector.Models
{
    using System.Collections.Generic;

    public class Advertiser
    {
        public string Type { get; set; }
        public int Id { get; set; }
        public string Name { get; set; }
        public string LogoUrl { get; set; }
        public string PreferredColourHex { get; set; }
        public string BannerUrl { get; set; }
        public List<Contact> Contacts { get; set; }
    }
}