namespace DomainApiConnector.Models
{
    public class Box
    {
        public Point TopLeft { get; set; }
        public Point BottomRight { get; set; }
    }
}